<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig){
    
    $settings = new admin_settingpage('local_mooring', "Mooring");
    $ADMIN->add('localplugins', $settings);
    
    $settings->add(new admin_setting_configtext(
            'local_mooring/slackurl',
            "Slack incoming webhooks",
            null,
            null,
            PARAM_URL
    ));
    
    $settings->add(new admin_setting_configtext(
            'local_mooring/slackchannel',
            "Slack channel",
            null,
            null,
            PARAM_ALPHANUMEXT
    ));
}