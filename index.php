<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Index
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(__DIR__)) . '/config.php');
defined('MOODLE_INTERNAL') || die();

defined('LOCAL_MOORING_ROOT') || define('LOCAL_MOORING_ROOT', __DIR__);
define('LOCAL_MOORING_PLUGIN', basename(dirname(LOCAL_MOORING_ROOT)) . '_' . basename(LOCAL_MOORING_ROOT));

$way = filter_input(INPUT_GET, 'way', FILTER_SANITIZE_STRING);

if ($way) {
    $way = explode('.', $way);
} else if (defined('LOCAL_MOORING_HOME')) {
    $way = explode('.', LOCAL_MOORING_HOME);
} else {
    $controller = new local_mooring\local\controllers\core_controller();
    $controller->not_found();
}

$controller = LOCAL_MOORING_PLUGIN . '\local\controllers\\' . $way[0] . '_' . $way[1];
$controller = new $controller();
($way[1] === 'ajax') ? $controller->ajax($way[2]) : $controller->{$way[2]}();
