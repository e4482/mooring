<?php

// This file is part of Mooring.
//
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Array of plugin parameters that can be changed by a webservice
 *
 * @package     local_mooring
 * @author      Charly Piva
 * @copyright   (C) Académie de Versailles 2018
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

return [
    'local_teacherboard'    => [
        'messagetextcolor',
        'messagebackgroundcolor',
        'message'
    ],
    'editor_atto'           => '*',
    'atto_collapse'         => '*',
    'atto_equation'         => '*',
    'atto_poodll'           => '*',
    'CORE'                  => [
    ]
];