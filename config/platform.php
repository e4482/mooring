<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Platforms configuration
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

return [
    
    "https://antony.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Jean-Philippe Cottu",
                "email"         => "jean-philippe.cottu@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://argenteuil.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Anissa Ben Fahdel",
                "email"         => "auns.ben-fadhel@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille Fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://boulogne.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Benoît Caillot",
                "email"         => "benoit.caillot@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://cergy.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Amélie Beney",
                "email"         => "amelie.beney@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille Fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://enghien.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Julien Delmas",
                "email"         => "julien.delmas@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://etampes.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Nour-Eddine El Yazghi",
                "email"         => "nour-eddine.el-yazghi@ac-versailles.fr"
            ],
            [
                "username"      => "manager2",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Bleuenn Thamin",
                "email"         => "bleuenn.thamin@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Isabelle Soriano",
                "email"         => "isabelle.soriano@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://evry.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Cyrille Bertrand",
                "email"         => "cyrille.bertrand@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Isabelle Soriano",
                "email"         => "isabelle.soriano@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://gennevilliers.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Pierre Bezanger",
                "email"         => "pierre.bezanger@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://gonesse.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Carl Mambourg",
                "email"         => "carl.mambourg@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille Fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://mantes.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Kévin Zanotti",
                "email"         => "kevin.zanotti@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://massy.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Pierre Jorland",
                "email"         => "pierre.jorland@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Isabelle Soriano",
                "email"         => "isabelle.soriano@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://montgeron.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Baptiste Chappaz",
                "email"         => "baptiste.chappaz@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Isabelle Soriano",
                "email"         => "isabelle.soriano@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://mureaux.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christian Lefebvre",
                "email"         => "christian.lefebvre@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://nanterre.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christophe Rigal",
                "email"         => "christophe.rigal@ac-versailles.fr"
            ],
            [
                "username"      => "manager2",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Benoît Caillot",
                "email"         => "benoit.caillot@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://neuilly.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Olivier Arcamone",
                "email"         => "olivier.arcamone@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://poissy.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Olivier Menard",
                "email"         => "olivier.menard@ac-versailles.fr"
            ],
            [
                "username"      => "manager2",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christian Lefebvre",
                "email"         => "christian.lefebvre@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://pontoise.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christophe Buissez",
                "email"         => "christophe.buissez@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille Fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://rambouillet.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Charlie Rollo",
                "email"         => "charlie.rollo@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://sarcelles.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christian Dickelé",
                "email"         => "christian.dickele@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Camille Fossiez",
                "email"         => "camille.fossiez@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://savigny.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Alain Grimault",
                "email"         => "alain.grimault@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Isabelle Soriano",
                "email"         => "isabelle.soriano@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://sgl.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Christophe Schoeser",
                "email"         => "christophe.schoeser@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://sqy.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Cyrille Dupuis",
                "email"         => "cyrille.dupuis@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://vanves.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Gregory Anguenot",
                "email"         => "gregory.anguenot@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Anne Ragimbeau",
                "email"         => "anne.ragimbeau@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://versailles.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Conseiller de bassin",
                "lastname"      => "Yann Vavasseur",
                "email"         => "yann.vavasseur@ac-versailles.fr"
            ],
            [
                "username"      => "managerdpt",
                "firstname"     => "Référente départementale",
                "lastname"      => "Aurélie Collet",
                "email"         => "aurelie.collet@ac-versailles.fr"
            ],
            [
                "username"      => "manager1d",
                "firstname"     => "Référent 1er degré",
                "lastname"      => "Adel Litaiem",
                "email"         => "adel.litaiem@ac-versailles.fr"
            ]
        ]
    ],
    
    "https://dev01.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Manager",
                "lastname"      => "Christophe Buissez",
                "email"         => "christophe.buissez@ac-versailles.fr"
            ]
        ]
    ],
    
    "http://dev.elea.ac-versailles.fr" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Manager",
                "lastname"      => "Rémi Lefeuvre",
                "email"         => "remi.lefeuvre@ac-versailles.fr"
            ],
            [
                "username"      => "manager2",
                "firstname"     => "Manager",
                "lastname"      => "Christophe Buissez",
                "email"         => "christophe.buissez@ac-versailles.fr"
            ]
        ]
    ],

    "http://localhost" => [
        "managers"  => [
            [
                "username"      => "manager",
                "firstname"     => "Manager",
                "lastname"      => "Rémi Lefeuvre",
                "email"         => "remi.lefeuvre@ac-versailles.fr"
            ],
            [
                "username"      => "manager2",
                "firstname"     => "Manager",
                "lastname"      => "Nicolas Daugas",
                "email"         => "nicolas.daugas@ac-versailles.fr"
            ]
        ]
    ]
];

