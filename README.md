# MOORING
## One plugin to rule them all !

-----

## Requirements

### Extended Fields

You must create 5 new text fields in user profil :

        1. profil
        2. uai
        3. siecle
        4. disambiguation
        5. timeretrieved

### Grunt CLI via npm (as root)

        apt-get install -y nodejs nodejs-legacy npm
        npm install -g grunt-cli

## Installation

### Minify AMD JS files (Massimilate & Platformagent included)

        npm install
        grunt install
        rm -rf node_modules

## Dev environment

### Watch for modification in amd/src/*.js files (Massimilate & Platformagent included)

        grunt
