<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Application controller
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\controllers;

class app_controller extends core_controller {
    
    protected $context;
    protected $slack = null;
    
    public function __construct() {
        $this->template = 'default';
        $this->viewpath = LOCAL_MOORING_ROOT . '/views/';
        $this->context = \context_system::instance();
    }
    
    protected function load_model($modelname, $varname = null) {
        if (!isset($varname)) {
            $varname = $modelname;
        }
        $model = LOCAL_MOORING_PLUGIN . '\local\models\\' . $modelname;
        if (!class_exists($model)) {
            $model = 'local_mooring\local\models\\' . $modelname;
        }
        $this->$varname = new $model();
    }
    
    public function ajax($func) {
        try {
            $response = new \stdClass();
            $response->data = $this->$func();
        } catch (\Exception $e) {
            $response->error = $e->getMessage();
            if ($this->slack !== null && $this->slack->available) {
                $this->slack->transmit_exception($e);
            }
        } finally {
            echo json_encode($response);
        }
    }
    
}
