<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Configuration class
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local;

class config {
    
    private $base = [];
    private $cas = [];
    private $user_field_ids = [];
    private $editableparams = [];
    private static $_instance;
    
    public static function load($filename = null) {
        if (self::$_instance == null) {
            self::$_instance = new config();
        }
        if (isset($filename) && empty(self::$_instance->$filename)) {
            self::$_instance->$filename = require dirname(dirname(__DIR__)) . '/config/' . $filename . '.php';
        }
        return self::$_instance;
    }

    public function get($key, $table = 'base') {
        $array = $this->$table;
        if (!isset($array[$key])) {
            return null;
        }
        return $array[$key];
    }
    
    public function get_all_cas() {
        return $this->cas;
    }
    
    public function get_user_field_id($shortname) {
        if (!isset($this->user_field_ids[$shortname])) {
            global $DB;
            $this->user_field_ids[$shortname] = $DB->get_record('user_info_field', [
                'shortname' => $shortname
            ], 'id', MUST_EXIST)->id;
        }
        return $this->user_field_ids[$shortname];
    }

    public function is_editable_param($plugin, $param){
        if(isset($this->editableparams[$plugin]) && $param != 'version'){
            if($this->editableparams[$plugin] == '*' || in_array($param,$this->editableparams[$plugin])){
                return true;
            }
        }
        return false;
    }
    
}

