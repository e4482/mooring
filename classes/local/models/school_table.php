<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School table model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

use local_mooring\local\models\core_table;

class school_table extends core_table {
    
    protected $table = 'local_mooring_school';
    protected $table2 = 'local_trainingtroops_sandbox';
    
    public function one($uai) {
        global $DB;
        if(in_array($this->table2,$DB->get_tables())){
            //School
            $school = $DB->get_record($this->table, ['uai' => $uai]);
            if($school) return $school; 

            //Sandbox
            $sandbox = $DB->get_record($this->table2, ['uai' => $uai], '*', MUST_EXIST);
            //Empty fields if retrieving a sandbox
            $sandbox->cas = '';
            $sandbox->city = '';
            $sandbox->nature = '';
            $sandbox->special = '';
            return $sandbox;
        }
        return $DB->get_record($this->table, ['uai' => $uai], '*', MUST_EXIST);
    }
    
    public function special($uai, $special) {
        global $DB;
        return $DB->set_field($this->table, 'special', $special, array(
            'uai'       =>  $uai,
            'special'    =>  null
        ));
    }
    
    public function exist($uai) {
        global $DB;
        $school = $DB->get_record($this->table, ['uai' => $uai]);
        if (!$school) {
            if(in_array($this->table2,$DB->get_tables())){
                $sandbox = $DB->get_record($this->table2, ['uai' => $uai]);
                if(!$sandbox){
                    return false;
                }
                return true;
            }
            return false;
        }
        return true;
    }
    
}
?>