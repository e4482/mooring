<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core slack model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class core_slack {
    
    protected $url;
    protected $channel;
    protected $username;
    protected $text = null;
    protected $fields = [];
    protected $attachments = [];
    
    public function text($text) {
        if ($text === null) {
            $this->text = $text;
        } else {
            $this->text = $this->text . $text;
        }
        return $this;
    }
    
    public function field($field) {
        if (!isset($field['short'])) {
            $field['short'] = true;
        }
        $this->fields[] = $field;
        return $this;
    }
    
    public function attachment($attachment) {
        if (!empty($this->fields)) {
            $attachment['fields'] = $this->fields;
            $this->fields = [];
        }
        $this->attachments[] = $attachment;
        return $this;
    }
    
    private function playload() {
        return rawurlencode(json_encode([
            'channel'       =>  '#' . $this->channel,
            'username'      =>  $this->username,
            'text'          =>  $this->text,
            'attachments'   =>  $this->attachments
        ]));
    }
    
    public function send() {
        $ch = curl_init($this->url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => 'payload=' . $this->playload()
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    
}

