<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Session model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class core_session {
    
    protected $array;
    
    public function __construct() {
        $this->array = strstr(get_called_class(), '_', true);
        if (!isset($_SESSION[LOCAL_MOORING_PLUGIN])) {
            $_SESSION[LOCAL_MOORING_PLUGIN] = [];
        }
        if (!isset($_SESSION[LOCAL_MOORING_PLUGIN][$this->array])) {
            $_SESSION[LOCAL_MOORING_PLUGIN][$this->array] = [];
        }
    }
    
    public function __get($name) {
        
        if (isset($_SESSION[LOCAL_MOORING_PLUGIN][$this->array][$name])) {
            return $_SESSION[LOCAL_MOORING_PLUGIN][$this->array][$name];
        } else {
            return false;
        }
    }
    
    public function __set($name, $value) {
        $_SESSION[LOCAL_MOORING_PLUGIN][$this->array][$name] = $value;
    }
    
    public function clear() {
        unset($_SESSION[LOCAL_MOORING_PLUGIN][$this->array]);
    }
    
}

