<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core user model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

use local_mooring\local\config;

class core_user {
    
    public function create($user) {
        global $CFG;
        
        if (!isset($user->mnethostid)) {
            $user->mnethostid = 1;
        }
        if (!isset($user->confirmed)) {
            $user->confirmed = 1;
        }
        
        require_once $CFG->dirroot . '/user/lib.php';
        $userid = \user_create_user($user, false);
        
        return $userid;
    }
    
    public function update($user) {
        global $CFG;
        
        if (!isset($user->suspended)) {
            $user->suspended = 0;
        }
        
        require_once $CFG->dirroot . '/user/lib.php';
        \user_update_user($user, false);
    }
    
    protected function set_fields($userid, $data) {
        foreach ($data as $key => $value) {
            $this->set_field($userid, $key, $value);
        }
    }
    
    protected function set_field($userid, $shortname, $data) {
        global $DB;
        $fieldid = $this->get_field_id($shortname);
        $record = $DB->get_record('user_info_data', [
            'fieldid'   => $fieldid,
            'userid'    => $userid
        ], 'id, data');
        if ($record === false) {
            $DB->insert_record('user_info_data', [
                'fieldid'   => $fieldid,
                'userid'    => $userid,
                'data'      => $data
            ]);
        } else if ($record->data != $data) {
            $DB->update_record('user_info_data', [
                'id'        => $record->id,
                'data'      => $data
            ]);
        }
    }
    
    protected function get_field_id($shortname) {
        return config::load()->get_user_field_id($shortname);
    }
    
}
