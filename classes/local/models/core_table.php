<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core table model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class core_table {
    
    protected $table;
    
    public function get_table() {
        return $this->table;
    }
    
    protected function to_entity($array) {
        $model = str_replace('_table', '_entity', get_class($this));
        return new $model($array);
    }
    
    protected function to_entities($array) {
        $objects = [];
        foreach ($array as $value) {
            $objects[] = $this->to_entity($value);
        }
        return $objects;
    }
    
}

