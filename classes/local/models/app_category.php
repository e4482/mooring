<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Application category model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class app_category {
    
    private static $table = 'course_categories';
    
    public static function get_id($uai) {
        global $DB;
        if(!$record = $DB->get_record(self::$table, ['idnumber' => $uai], 'id')) {
            return self::create($uai);
        } else {
            return $record->id;
        }
    }
    
    private static function create($uai) {
        global $CFG;
        require_once($CFG->libdir . '/coursecatlib.php');
        return \coursecat::create(['name' => $uai, 'idnumber' => $uai])->id;
    }
    
}

