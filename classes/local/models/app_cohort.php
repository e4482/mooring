<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Application cohort model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class app_cohort {
    
    private $table = 'cohort';
    private $contextid;
    
    public function __construct($uai) {
        $this->contextid = \context_coursecat::instance(app_category::get_id($uai))->id;
    }
    
    public function all() {
        global $DB;
        return $DB->get_records($this->table, [
            'contextid' => $this->contextid,
            'idnumber'  => 'mooring'
        ], 'name', 'id, name');
    }
    
    public function all_indexed_by_name() {
        $records = $this->all();
        $cohorts = [];
        foreach ($records as $record) {
            $cohorts[$record->name] = $record->id;
        }
        return $cohorts;
    }
    
    public function one($name) {
        global $DB;
        return $DB->get_record($this->table, [
            'contextid' => $this->contextid,
            'name'      => $name,
            'idnumber'  => 'mooring'
        ], 'id, name');
    }
    
    public function create($name) {
        global $CFG;
        $cohort = (object) [
            'contextid' => $this->contextid,
            'name'      => $name,
            'idnumber'  => 'mooring'
        ];
        require_once($CFG->dirroot . '/cohort/lib.php');
        return cohort_add_cohort($cohort);
    }
    
    private function get_cohorts_ids($cohorts, $allcohorts = null) {
        if ($allcohorts === null) {
            $allcohorts = $this->all_indexed_by_name();
        }
        $ids = [];
        foreach ($cohorts as $name) {
            if (isset($allcohorts[$name])) {
                $ids[] = $allcohorts[$name];
            } else {
                $ids[] = $this->create($name);
            }
        }
        return $ids;
    }
    
    private function get_user_cohortids($userid) {
        global $DB;
        $records = $DB->get_records('cohort_members', ['userid' => $userid], '', 'cohortid');
        $cohorts = [];
        foreach ($records as $record) {
            $cohorts[] = $record->cohortid;
        }
        return $cohorts;
    }
    
    public function update_user($userid, $cohorts, $allcohorts = null) {
        $ids = $this->get_cohorts_ids($cohorts, $allcohorts);
        $actual = $this->get_user_cohortids($userid);
        foreach ($actual as $id) {
            if (!in_array($id, $ids)) {
                $this->unsuscribe_user($userid, $id);
            }
        }
        foreach ($ids as $id) {
            $this->suscribe_user($userid, $id);
        }
    }

    public function get_users_from_cohort($cohortid) {
        global $DB;

        $sql = 'SELECT u.id, firstname, lastname
                FROM {user} u
                INNER JOIN {cohort_members} cm
                ON u.id = cm.userid
                WHERE cm.cohortid = ?
                ORDER BY u.lastname';
        return $DB->get_records_sql($sql, [$cohortid]);
    }
    
    private function suscribe_user($userid, $cohortid) {
        global $CFG;
        require_once($CFG->dirroot . '/cohort/lib.php');
        cohort_add_member($cohortid, $userid);
    }
    
    public function unsuscribe_user($userid, $cohortid) {
        global $CFG;
        require_once($CFG->dirroot . '/cohort/lib.php');
        cohort_remove_member($cohortid, $userid);
    }
    
}

