<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Navigation class
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local;

class nav {
    
    private static $instance;
    private static $class = __CLASS__;
    
    public static function load() {
        if (self::$instance === null) {
            self::$instance = new self::$class();
        }
        return self::$instance;
    }
    
    private $nav;
    private $rootnodes = [];
    private $keys = [
        'admin'     => "Administration de la plateforme",
        'platform'  => "Gestion de la plateforme",
        'school'    => "Gestion de l'établissement"
    ];
    
    public function extend_nav($nav, $context, $nodes) {
        $this->nav = $nav;
        foreach ($nodes as $node) {
            if (has_capability($node['capability'], $context)) {
                $this->add_child_node($node);
            }
        }
    }
    
    private function get_root_node($nodename) {
        if(!isset($this->rootnodes[$nodename])) {
            $this->rootnodes[$nodename] = $this->nav->add($this->keys[$nodename]);
        }
        return $this->rootnodes[$nodename];
    }
    
    private function add_child_node($node) {
        $murl = new \moodle_url($node['url'], $node['params']);
        $rootnode = $this->get_root_node($node['rootnode']);
        $childnode = $rootnode->add($node['text'], $murl);
        if ($_SERVER['REQUEST_URI'] === $murl->out_as_local_url(false)) {
            $childnode->make_active();
        }
    }
    
}

