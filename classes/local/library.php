<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library class
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local;

class library {
    
    public static function test_uai($uai, $e = true) {
        $exp = '/^([0-9]{7})([a-hj-npr-z]{1})$/i';
        $matches = [];
        if (!preg_match($exp, $uai, $matches)) {
            if ($e) {
                throw new \Exception("UAI: 7 chiffres suivis d'une lettre");
            } else {
                return false;
            }
        }
        // les lettres i, o et q ne sont pas utilisées
        $CHARS = 'abcdefghjklmnprstuvwxyz';
        $modulo = $matches[1] % strlen($CHARS);
        if (strpos($CHARS, strtolower($matches[2])) !== $modulo) {
            if ($e) {
                throw new \Exception("UAI: clé erronée");
            } else {
                return false;
            }
        }
        return true;
    }
    
    public static function timeago($timestamp) {
        if ($timestamp === 0) {
            return "jamais";
        }
        $s = time() - $timestamp;
        if ($s < 1) {
            return "à l'instant";
        }
        $levels = [
            12 * 30 * 24 * 60 * 60  =>  "année",
            30 * 24 * 60 * 60       =>  "mois",
            24 * 60 * 60            =>  "jour",
            60 * 60                 =>  "heure",
            60                      =>  "minute",
            1                       =>  "seconde"
        ];
        foreach ($levels as $value => $name) {
            $d = $s / $value;
            if ($d >= 1) {
                $r = round($d);
                $timeago = "il y a " . $r . " " . $name;
                if ($r > 1 && $name !== "mois") {
                    $timeago .= "s";
                }
                return $timeago;
            }
        }
    }
    
    public static function school_import_source($school) {
        if ($school->cas) {
            return config::load('cas')->get($school->cas, 'cas')['import'];
        } else if ($school->nature === 'ecole') {
            return 'primaire';
        } else {
            return 'secondaire';
        }
    }
    
    public static function school_import_method($school) {
        if ($school->cas) {
            return $school->cas;
        } else if ($school->nature === 'ecole') {
            return 'primaire';
        } else {
            return 'secondaire';
        }
    }
    
    public static function is_any_cas($uai, $nature) {
        $cas = null;
        $dept = (int) substr($uai, 0, 3);
        foreach (config::load('cas')->get_all_cas() as $casname => $settings) {
            if (self::test_cas_criteria($settings['criteria'], compact('uai', 'nature', 'dept'))) {
                $cas = $casname;
                break;
            }
        }
        return $cas;
    }
    
    private static function test_cas_criteria($criteria, $variables) {
        $bool = true;
        extract($variables);
        foreach ($criteria as $type => $odds) {
            if (!in_array($$type, $odds)) {
                $bool = false;
                break;
            }
        }
        return $bool;
    }
    
}